1. Install node 6.11.0
2. Run `npm install`
3. Run `bower install`
4. Run `gulp`
5. Run `gulp watch`

Add this line of code to your vhosts
```
<VirtualHost *:80>
   DocumentRoot "C:/xampp/htdocs/projects/gosi"
   ServerName www.gosi.com
   <Directory "C:/xampp/htdocs/projects/gosi">
       Options All
       AllowOverride All
       Require all granted
   </Directory>
</VirtualHost>
```

Add this line of code to your Hosts
```
127.0.0.1 www.gosi.com
```
