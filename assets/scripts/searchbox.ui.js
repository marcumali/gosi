var searchBoxFnc = {};

( function( $ ){

    searchBoxFnc.Animation = function(){
        $( '.js-focus' ).focus(function(){
            $( this ).parent().addClass( 'is-focus' );
        }).blur(function(){
            $( this ).parent().removeClass( 'is-focus' );

        });
    };

    searchBoxFnc.Animation();

}) (jQuery);
