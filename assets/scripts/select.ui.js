var selectFnc = {};

( function( $ ){

    $trigger = $( '.js-select' );
    $hiddenItems = $( '.select-hidden-items' );

    selectFnc.Dropdown = function(){

        $.each( $( '.select-active' ), function( index, value ){

            var _this = $( value );
            _this
                .text(
                    _this
                        .parents( '.select-case' )
                        .find( '.selected' )
                        .text()
                );

        });

        $.each( $trigger, function( index, value ){

            var _this = $( value );
            _this.on( 'click', function(){
                $hiddenItems.stop().slideUp();
                _this
                    .parents( '.select-case' )
                    .find( $hiddenItems )
                    .stop()
                    .slideToggle();
            });

        });

        $.each( $hiddenItems.find( 'li' ), function( index, value ){

            var _this = $( value );

            _this.on( 'click', function(){
                var select = _this.parents( '.select-case' );
                select
                    .find( 'li' ).removeClass( 'selected' );
                select
                    .find( '.select-active' ).text( _this.text() );
                _this
                    .addClass( 'selected' );
                select
                    .find( $hiddenItems ).slideUp();
            });

        });

        $( document ).on( 'mouseup',function (e) {

            var hiddenItems = $( '.select-hidden-items' );

            if ( !$( '.js-select' ).is( e.target ) && !hiddenItems.is( e.target ) && hiddenItems.has( e.target ).length === 0 ) {
                hiddenItems.slideUp();
            }

        });

    };

    selectFnc.Dropdown();

}) (jQuery);
