var modalJs = {};

( function($){

	function getScrollBarWidth () {
		var outer = document.createElement("div");
		outer.style.visibility = "hidden";
		outer.style.width = "100px";
		document.body.appendChild(outer);

		var widthNoScroll = outer.offsetWidth;

		outer.style.overflow = "scroll";

		var inner = document.createElement("div");
		inner.style.width = "100%";
		outer.appendChild(inner);

		var widthWithScroll = inner.offsetWidth;

		outer.parentNode.removeChild(outer);

		return widthNoScroll - widthWithScroll;
	}

	modalJs.init = function(){

		var $container = $( '.modal' );
		var modalContent = $( '.modal-content' );

		$( '.js-show-modal' ).css({ 'cursor':'pointer' });

		$( document ).on( 'click keydown', '.js-show-modal', function(){

			$container.fadeIn().addClass( 'is-show' );
			$( 'body' ).addClass( 'modal-open' );
			// $( '.modal-open' ).css({ 'padding-right':getScrollBarWidth() });

			$( '.modal-container' ).addClass( 'is-show' );

			$( '.modal-container:visible' ).hide();
			$( '#' + $( this ).attr( 'data-modal-id' ) ).show();

		});

		function modalOut(){

			$container.fadeOut().removeClass( 'is-show' );
			$( '.modal-container' ).removeClass( 'is-show' );
			$( 'body' ).removeAttr( 'style' ).removeClass( 'modal-open' );

			modalContent.fadeIn();

		}

		$( '.js-modal-close' ).on( 'click keydown', function() {
			modalOut();
		});

		$( '.modal' ).on( 'click keydown', function(e) {
			if (e.target !== this){
				return;
			}
			modalOut();
		});

	};

	modalJs.init();

}) (jQuery);
